package com.centralesupelec.calendars.calendars.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centralesupelec.calendars.calendars.usefullObject.Association;
import com.centralesupelec.calendars.calendars.R;
import com.centralesupelec.calendars.calendars.database.AssociationsDAO;
import com.centralesupelec.calendars.calendars.database.HappeningDAO;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import static java.util.Collections.sort;


public class SelectActivity extends AppCompatActivity implements View.OnClickListener {
    // Liste des associations, qui doit être mise à jour via AddActivity
    public Vector<Association> listAsso;
    // Liste des cases à cocher
    public Vector<CheckBox> listCases;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        // Récupération des données sauvegardées
        SharedPreferences sp = getSharedPreferences("data", Context.MODE_PRIVATE);



        //on rajoute toutes les assos
        AssociationsDAO.open(this);
        listAsso = AssociationsDAO.getAsso();
        AssociationsDAO.close();

        LinearLayout ll;
        ll = findViewById(R.id.LinearLayoutSelect);

        // Pour chaque association, on affiche une case que l'on peut cocher
        listCases = new Vector<>();

        for (int i=0; i<listAsso.size(); i++) {
            CheckBox c = new CheckBox(this);
            c.setId(i);
            if (sp.contains("idAsso")) {
                Set<String> idAsso = sp.getStringSet("idAsso", new HashSet<String>());
                if (idAsso.contains(Integer.toString(listAsso.get(i).getId()))) {
                    c.setChecked(true);
                }
            }

            String assoName = listAsso.elementAt(i).getName();
            c.setText(assoName);
            ll.addView(c);
            listCases.add(c);

            c.setClickable(true);
            c.setOnClickListener(this);
        }
        // Return button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Add white space to allow scrolling to the end
        TextView tvSpace = new TextView(this);
        tvSpace.setHeight(250);
        ll.addView(tvSpace);
    }

    @Override
    public void onClick(View v) {
        saveCheck();
    }

    public void saveCheck() {
        SharedPreferences sp = getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();        // to save asso of interest
        Set<String> sIdAsso = new HashSet<>();

        for (int i=0; i<listAsso.size(); i++) {
            // Add id to list if checked
            if (listCases.elementAt(i).isChecked()) {
                sIdAsso.add(Integer.toString(listAsso.get(i).getId()));
            }
        }
        editor.putStringSet("idAsso", sIdAsso);
        editor.apply();
    }

    public void goToCal(View view) {
        finish();
    }

    public void goToAdd(View view) {
        Intent toAdd = new Intent();
        toAdd.setClass(this, AddActivity.class);
        startActivity(toAdd);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AssociationsDAO.open(this);
        HappeningDAO.open(this);
    }

    @Override
    protected void onPause() {
        AssociationsDAO.close();
        HappeningDAO.close();
        super.onPause();
    }


    // Return button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}



