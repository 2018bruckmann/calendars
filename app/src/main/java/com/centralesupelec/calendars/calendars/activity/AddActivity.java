package com.centralesupelec.calendars.calendars.activity;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.centralesupelec.calendars.calendars.usefullObject.Association;
import com.centralesupelec.calendars.calendars.picker.DatePickerFragment;
import com.centralesupelec.calendars.calendars.picker.DurationPickerFragment;
import com.centralesupelec.calendars.calendars.R;
import com.centralesupelec.calendars.calendars.picker.TimePickerFragment;
import com.centralesupelec.calendars.calendars.database.AssociationsDAO;
import com.centralesupelec.calendars.calendars.database.HappeningDAO;

import java.util.Date;
import java.util.Vector;
import java.util.regex.Pattern;

/**
 * Classe représentant l'activité AddActivity, pour ajouter un événement dans la base de données
 * */
public class AddActivity extends AppCompatActivity {

    //Pour stocker les Views dont on a besoin
    EditText nameEdit;
    EditText descriptionEdit;
    Spinner associationsSpinner;
    EditText startDateEdit;
    EditText startHourEdit;
    EditText durationEdit;
    Button button;

    //Pour stocker les informations dont on a besoin depuis la bdd
    Vector<String> associationsNames;
    Vector<Integer> associationsIds;

    //Pour stocker l'id de l'association sélectionnée dans le spinner
    int associationId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        //On récupère les Views dont on a besoin
        nameEdit = findViewById(R.id.name);
        descriptionEdit = findViewById(R.id.description);
        associationsSpinner = findViewById(R.id.associations_spinner);
        startDateEdit = findViewById(R.id.start_date);
        startHourEdit = findViewById(R.id.start_hour);
        durationEdit = findViewById(R.id.duration);
        button = findViewById(R.id.add_button);

        //On initialise les vecteurs pour y ajouter des éléments plus tard
        associationsNames = new Vector<>();
        associationsIds = new Vector<>();

        //On initialise l'id de l'association sélectionée à -1 (aucune asso sélectionnée)
        associationId = -1;

        //Récupération de la liste des associations depuis la base de données
        AssociationsDAO.open(this);
        Vector<Association> associations = AssociationsDAO.getAsso();
        AssociationsDAO.close();

        //Ajout des associations (noms et ids dans les vecteurs correspondant
        for (Association asso : associations) {
            associationsNames.add(asso.getName());
            associationsIds.add(asso.getId());
        }

        //On ajoute "Association" comme "hint" de la liste déroulante et on transforme le vecteur en liste.
        associationsNames.insertElementAt(getString(R.string.association), 0);
        String[] listAssociations = associationsNames.toArray(new String[associationsNames.size()]);

        //A chaque changement du nom, on revérifie si les champs sont valides.
        nameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkFields(); //Fonction de vérification des champs
            }
        });

        //Création de l'adapter (liste des éléments à afficher dans la liste déroulante)
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listAssociations) {
            @Override
            public boolean isEnabled(int position){
                if(position == 0) {
                    // Désactivation du premier élément (hint)
                    return false;
                }
                else {
                    return true;
                }
            }

            //Ouverture de la liste déroulante
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                // Fermeture du clavier
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;

                if(position == 0){
                    // Affichage du hint en gris
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    //Affichage des vraies options en noir
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Récupération de l'id de l'association sélectionnée.
        associationsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    //La position 0 représente le "hint", il y a donc un décalage
                    associationId = associationsIds.get(position - 1);
                } else {
                    //"Association" est sélectionnée
                    associationId = -1;
                }
                //Vérification des champs
                checkFields();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Rien n'est sélectionné
                associationId = -1;
            }
        });

        //Mise en place de la liste déroulante.
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        associationsSpinner.setAdapter(spinnerAdapter);

        //Bouton retour (en haut à droite)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Affiche un calendrier pour sélectionner la date  de début de l'événement
     *
     * @param view
     * */
    public void showDatePickerDialog(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    /**
     * Affiche une horloge pour sélectionner l'heure de début de l'événement
     *
     * @param view
     * */
    public void showTimePickerDialog(View view) {
            DialogFragment newFragment = new TimePickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    /**
     * Affiche une horloge pour sélectionner la durée de l'événement
     * */
    public void showDurationPickerDialog(View view) {
        DialogFragment newFragment = new DurationPickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    /**
     * Vérifie que les données entrées sont correctes et modifie le bouton pour ajouter en fonction :
     *     - Si elles ne le sont pas, le bouton n'est plus cliquable
     *     - Sinon le bouton redevient cliquable
     * */
    public void checkFields() {

        //Récupération des entrées
        String name = nameEdit.getText().toString();
        String description = descriptionEdit.getText().toString();
        String startDateString = startDateEdit.getText().toString();
        String startHourString = startHourEdit.getText().toString();
        String durationString = durationEdit.getText().toString();

        //Vérification du bon format des inputs
        Pattern nonEmptyPattern = Pattern.compile("^[^;\\s]([^;\\s]|[ ])*$"); //Pattern pour le  nom
        Pattern datePattern = Pattern.compile("^[\\d]{2}\\/[\\d]{2}\\/[\\d]{4}$"); //Pattern pour la date
        Pattern hourPattern = Pattern.compile("^([0-2][0-9]|[\\d])[h][0-5][\\d]$"); //Pattern pour l'heure et la durée

        //Si tous les champs sont corrects
        if (nonEmptyPattern.matcher(name).matches()
                && datePattern.matcher(startDateString).matches()
                && hourPattern.matcher(startHourString).matches()
                && hourPattern.matcher(durationString).matches()
                && associationId != 0) {

            //On active le bouton et met le texte qui indique que l'on peut ajouter.
            button.setClickable(true);
            button.setText(R.string.add);
        } else {

            //On désactive le bouton et on affiche le texte qui indique de remplir les zones correctement.
            button.setClickable(false);
            button.setText(R.string.complete);
        }
    }

    /**
     * Méthode onClick du bouton : Ajoute les données entrées (événement) à la base de données.
     *
     * @param view
     * */
    public void add(View view) {

        //Récupération des données
        String name = nameEdit.getText().toString();
        String description = descriptionEdit.getText().toString();
        String startDateString = startDateEdit.getText().toString();
        String startHourString = startHourEdit.getText().toString();
        String durationString = durationEdit.getText().toString();

        //Récupération de l'objet date correspondant
        String[] startDateData = startDateString.split("/");
        String[] startHourData = startHourString.split("h");
        int day = Integer.valueOf(startDateData[0]);
        int month = Integer.valueOf(startDateData[1]);
        int year = Integer.valueOf(startDateData[2]);
        int hour = Integer.valueOf(startHourData[0]);
        int minute = Integer.valueOf(startHourData[1]);
        Date startDate = new Date(year-1900, month-1, day, hour, minute);

        //Récupération du timestamp correspondant à la durée
        String[] durationData = durationString.split("h");
        long durationTimeStamp = Integer.parseInt(durationData[0])*3600000 + Integer.parseInt(durationData[1])*60000;

        //Calcul des timestamps de début et fin d'après les entrées de l'utilisateur
        long startTimeStamp = startDate.getTime();
        long endTimeStamp = startTimeStamp + durationTimeStamp;

        //Conversion en objet Date de la date de fin
        Date endDate = new Date(endTimeStamp);


        //Enregistrement des données dans la base de données
        String associationName = associationsNames.elementAt(associationsIds.indexOf(associationId) + 1); //Décalage dû au premier élément "Association"
        HappeningDAO.insertValue(name, description, startDate, endDate, new Association(associationId, associationName));

        //On retourne à l'activité précédente.
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Ouverture des curseurs
        AssociationsDAO.open(this);
        HappeningDAO.open(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Fermeture des curseurs
        AssociationsDAO.close();
        HappeningDAO.close();
    }

    //Bouton retour (en haut à droite)
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
