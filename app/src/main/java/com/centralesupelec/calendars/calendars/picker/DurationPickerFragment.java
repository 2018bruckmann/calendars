package com.centralesupelec.calendars.calendars.picker;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.centralesupelec.calendars.calendars.activity.AddActivity;
import com.centralesupelec.calendars.calendars.R;

/**
 * Classe permettant l'affichage d'une pour la sélection de la durée de l'événement.
 * */
public class DurationPickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //On initialise l'horloge avec un temps de 0
        int hour = 0;
        int minute = 0;

        //On affiche un Toast pour demander la durée
        Toast.makeText(getContext(), getString(R.string.choose_duration), Toast.LENGTH_SHORT).show();

        //On ferme le clavier
        InputMethodManager imm = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

        //On renvoit une nouvelle instance de TimePicker
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    /**
     * Fonction appelée lorsque la sélection de la durée a été faite.
     * */
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        //On récupère l'EditText correspondant à l'entrée de la durée.
        EditText et = getActivity().findViewById(R.id.duration);

        //On rajoute une 0 devant le nombre de minutes s'il est inférieur à 10
        String minute_str = (minute < 10) ? "0" + minute : Integer.toString(minute);

        //On met la valeur obtenue dans la zone correspondant à la durée
        et.setText(String.format(getString(R.string.hour_format), hourOfDay, minute_str));

        //On appelle la fonction de vérification des champs
        ((AddActivity) getActivity()).checkFields();
    }
}