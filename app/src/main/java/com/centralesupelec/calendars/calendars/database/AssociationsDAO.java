package com.centralesupelec.calendars.calendars.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.centralesupelec.calendars.calendars.usefullObject.Association;

import java.util.Vector;

/**
 * On créé une classe qui permet de gérer la table des associations
 */
public abstract class AssociationsDAO{
    /*
    On donne le nom de la table et des atributs
     */
    public static final String TABLE_NAME = "Associations";
    public static final String KEY = "_id";
    public static final int ID_KEY = 0;
    public static final String NOM ="nom";
    public static final int ID_NOM = 1;
    public static final String[] COLUMNS ={KEY,NOM};
    // On créer la requete de création de la table
    public static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +" ( "
            + KEY +" INTEGER PRIMARY KEY, "+ NOM + " TEXT);";

    private static SQLiteDatabase db;




    /**
     * A utiliser au début d'une activité: permet d'ouvrir la BDD
     * @param context
     */
    public static void open(Context context) throws SQLiteException {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = dbHelper.getWritableDatabase();
        }
    }


    /**
     * Renvoie un curseur avec toute la table
     * @return cursor
     */
    public static Cursor readAll(){
        Cursor cursor = db.query(TABLE_NAME,COLUMNS,null,null,null,null,null,null);
        return cursor;
    }


    /**
     * On récupère le nom de l'asso et on l'ajoute à la base de donnée : on récupère son id
     * renvoit -1 si l'insertion est un échec
     * @param name
     * @return id
     */
    public static long insertValue(String name){
        ContentValues values = new ContentValues();
        values.put(NOM,name);
        long result = db.insert(TABLE_NAME,null ,values);
        return result;
    }

    /**
     * Donne le nombre de ligne dans le tableau
     * @return
     */


    public static int countAll(){
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM "+TABLE_NAME,null);
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static String getNameFromId(int id){

        String[] column = {NOM};
        String[] value = {Integer.toString(id)};
        Cursor cursor =db.query(TABLE_NAME,column,"_id = ?", value,null,null,null);
        cursor.moveToNext();
        String name = cursor.getString(0);

        return name;
    }



    public static Vector<Association> getAsso(){
        Cursor cursor = AssociationsDAO.readAll();
        Vector<Association> listAsso = new Vector<>();
        while (cursor.moveToNext()){
            listAsso.add(new Association(cursor.getInt(AssociationsDAO.ID_KEY),
                    cursor.getString(AssociationsDAO.ID_NOM)));
        }
        return listAsso;
    }

    public static void close(){
        db.close();
    }
    }


