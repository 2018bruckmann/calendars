package com.centralesupelec.calendars.calendars.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.centralesupelec.calendars.calendars.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    /**
     * On rajoute en local le pseudo renseigné par l'utilisateurs
     * @param view
     */
    public void registerUsername(View view) {
        SharedPreferences sp = getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String username = ((EditText) findViewById(R.id.username)).getText().toString();
        editor.putString("Username",username);
        editor.apply();
        Intent intent = new Intent(this, CalendarActivity.class);
        startActivity(intent);
        finish();

    }
}
