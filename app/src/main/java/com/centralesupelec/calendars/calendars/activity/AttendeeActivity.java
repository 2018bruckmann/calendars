package com.centralesupelec.calendars.calendars.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centralesupelec.calendars.calendars.R;
import com.centralesupelec.calendars.calendars.usefullObject.User;

import java.util.ArrayList;

public class AttendeeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendee);

        Intent fromCalAct = getIntent();
        ArrayList<User> vAttendee = (ArrayList<User>) fromCalAct.getSerializableExtra("attendee");

        LinearLayout ll = findViewById(R.id.attLayout);
        // Display description
        TextView tvDesc = new TextView(this);
        tvDesc.setText(fromCalAct.getStringExtra("desc"));
        tvDesc.setTextSize(20);
        ll.addView(tvDesc);
        // Add all attendee
        for (User a:vAttendee) {
            TextView tv = new TextView(this);
            tv.setText("+ " + a.getName());
            tv.setTextSize(20);
            ll.addView(tv);
        }

        // Return button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // Return button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
