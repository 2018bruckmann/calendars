package com.centralesupelec.calendars.calendars.usefullObject;

import java.io.Serializable;

import com.centralesupelec.calendars.calendars.database.AssociationsDAO;

import java.util.Vector;

public class Association implements Serializable {

    private String name;
    private String tag;
    private int id;
    private Vector<Happening> happenings;

    /**
     * Si on crée une association avec juste son nom c'est que l'on veut la mettre dans la basse
     * de donnée
     * @param name
     */
    public Association(String name) {
        this.name = name;
        this.tag = name.substring(0, 2); //Trois premières lettres
        this.happenings = new Vector<>();
        id = (int) AssociationsDAO.insertValue(name);
    }

    /**
     * Si on renseigne le nom et l'id, c'est que l'on veut créer l'objet à partir de la bdd, pas
     * besoin de l'insérer.
     * @param id
     * @param name
     */
    public Association(int id, String name){
        this.id=id;
        this.name=name;
    }

    Association(String name, Vector<Happening> happenings) {
        this.name = name;
        this.tag = name.substring(0, 2); //Trois premières lettres
        this.happenings = happenings;
    }


    public String getName() {
        return name;
    }

    public String getTag() {
        return tag;
    }

    public int getId() {
        return id;
    }

    public Vector<Happening> getHappenings() {
        return happenings;
    }

    public void addHappening(Happening h){
        this.happenings.add(h);
    }

}